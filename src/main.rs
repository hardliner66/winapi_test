mod helper;
mod wlan;

fn main() {
    match wlan::WlanService::new() {
        Ok(wlan_service) => {
            if let Ok(adapters) = wlan_service.get_adapters() {
                for adapter in adapters {
                    println!("{:#?}", adapter);
                }
            }
        }
        Err(error) => println!("Could not create wlan service: {}", error),
    }
}
