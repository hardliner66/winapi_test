extern crate winapi;

use self::winapi::shared::minwindef::DWORD;
use self::winapi::shared::ntdef::NULL;
use self::winapi::shared::winerror::*;
use self::winapi::um::winnt::HANDLE;
use self::winapi::um::wlanapi::*;

#[derive(Debug, Clone)]
pub enum WlanInterfaceState {
    NotReady,
    Connected,
    AdHocNetworkFormed,
    Disconnecting,
    Disconnected,
    Associating,
    Discovering,
    Authenticating,
    Unknown,
}

#[derive(Debug, Clone)]
pub struct WlanAdapter {
    pub id: String,
    pub description: String,
    pub interface_state: WlanInterfaceState,
}

pub struct WlanService {
    m_client: HANDLE,
}

const DW_MAX_CLIENT: DWORD = 2;

impl WlanService {
    pub fn new() -> Result<WlanService, String> {
        let mut handle = NULL;
        let mut dw_cur_version: DWORD = 0;
        match unsafe { WlanOpenHandle(DW_MAX_CLIENT, NULL, &mut dw_cur_version, &mut handle) } {
            ERROR_SUCCESS => Ok(WlanService { m_client: handle }),
            err => Err(format!("WlanOpenHandle failed with error: {}", err)),
        }
    }

    #[allow(non_upper_case_globals)]
    pub fn get_adapters(&self) -> Result<Vec<WlanAdapter>, String> {
        let mut p_if_list: PWLAN_INTERFACE_INFO_LIST = unsafe { std::mem::uninitialized() };
        match unsafe { WlanEnumInterfaces(self.m_client, NULL, &mut p_if_list) } {
            ERROR_SUCCESS => {
                let if_list = unsafe { *p_if_list };
                let count = if_list.dwNumberOfItems as usize;

                Ok((0..count)
                    .map(|i| if_list.InterfaceInfo[i])
                    .map(|info| {
                        Some(WlanAdapter {
                            id: ::helper::from_winapi_guid(&info.InterfaceGuid)
                                .to_simple()
                                .to_string(),
                            description: ::helper::from_winapi_string(&info.strInterfaceDescription),
                            interface_state: match info.isState {
                                wlan_interface_state_not_ready => WlanInterfaceState::NotReady,
                                wlan_interface_state_connected => WlanInterfaceState::Connected,
                                wlan_interface_state_ad_hoc_network_formed => {
                                    WlanInterfaceState::AdHocNetworkFormed
                                }
                                wlan_interface_state_disconnecting => {
                                    WlanInterfaceState::Disconnecting
                                }
                                wlan_interface_state_disconnected => {
                                    WlanInterfaceState::Disconnected
                                }
                                wlan_interface_state_associating => WlanInterfaceState::Associating,
                                wlan_interface_state_discovering => WlanInterfaceState::Discovering,
                                wlan_interface_state_authenticating => {
                                    WlanInterfaceState::Authenticating
                                }
                                _ => WlanInterfaceState::Unknown,
                            },
                        })
                    }).flatten()
                    .collect())
            }
            err => Err(format!("WlanEnumInterfaces failed with error: {}", err)),
        }
    }
}