extern crate uuid;
extern crate winapi;

use self::uuid::Uuid;
use self::winapi::shared::guiddef::GUID;

pub fn from_winapi_string(slice: &[u16]) -> String {
    String::from_utf16_lossy(
        slice
            .iter()
            .map(|i| *i)
            .take_while(|i| *i != 0)
            .collect::<Vec<u16>>()
            .as_slice(),
    )
}

pub fn from_winapi_guid(guid: &GUID) -> Uuid {
    Uuid::from_fields(
        guid.Data1 as u32,
        guid.Data2 as u16,
        guid.Data3 as u16,
        &(guid.Data4 as [u8; 8]),
    ).expect("Invalid GUID Data!")
}